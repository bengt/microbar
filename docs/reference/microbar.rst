microbar
========

.. testsetup::

    from microbar import *

.. automodule:: microbar
    :members:
