
Changelog
=========

0.1.0 (2016-09-02)
-----------------------------------------

* First release on PyPI.

0.2.0 (2016-09-02)
-----------------------------------------

* Integrations of CI services.

0.3.0 (2016-09-03)
-----------------------------------------

* First usable version, full test suite.
